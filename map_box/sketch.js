
//12.9716° N, 77.5946° E
//28.7041° N, 77.1025° E
var lat = 28.7041;
var lon = 77.1025;

//20.5937° N, 78.9629° E
var clat = 20.5937;
var clon = 78.9629;

var zoomLevel =1;

var earthquakes;
var img;
var cities;

function preload(){
    img = loadImage("https://api.mapbox.com/styles/v1/mapbox/dark-v9/static/"+clon+","+clat+","+zoomLevel+",0,0/1024x512?access_token=pk.eyJ1IjoiYW51cmFnc2hhcyIsImEiOiJjajh1NHRtM3AwdW5uMzJwOHRmc3V5MnZmIn0.2mFgEJTFJ8SUricAOcmoPw");
    earthquakes = loadStrings("./all_month.csv");
    cities = loadStrings("./simplemaps-worldcities-basic.csv");
}

function setup() {
	createCanvas(1024,512);
    translate(width/2,height/2);
    imageMode(CENTER);
    image(img,0,0);
    mapEarthquakes();
    mapCities();
}

function draw() {
}

function mercX(nlon){
    nlon = radians(nlon);
    var a = 256/PI*pow(2,zoomLevel);
    var b = nlon+PI;
    return a*b;
}

function mercY(nlat){
    nlat = radians(nlat);
    var a = 256/PI*pow(2,zoomLevel);
    var b = tan(PI/4+nlat/2);
    var c = PI - log(b);
    return a*c;
}

function mapEarthquakes(){
    stroke(255,0,255,200);
    fill(255,0,255,200);
    for(var i=0;i<earthquakes.length;i++){
        var data = earthquakes[i].split(/,/);
        var nlat = data[1];
        var nlon = data[2];
        var mag = data[4];
        mag = pow(10,mag);
        mag = sqrt(mag);
        var magmax = sqrt(pow(10,10));
        var d = map(mag,0,magmax,0,500);
        showPlace(nlat,nlon,d);
    }
}

function mapCities(){
    stroke(255,215,0,200);
    fill(255,215,0,200);
    var ppl = [];
    for(var i=1;i<cities.length;i++){
        var data = cities[i].split(/,/);
        ppl.push(parseFloat(data[4]));
        }
    var maxpop = ppl.reduce(function(a, b) {
        return Math.max(a, b);
    })
    for(var i=1;i<cities.length;i++){
        var data = cities[i].split(/,/);
        var nlat = data[2];
        var nlon = data[3];
        var x = ppl[i];
        var d =  map(x,0,maxpop,0,zoomLevel*6);
        showPlace(nlat,nlon,d);
    }
}

function showPlace(nlat,nlon,mag){
    var cx = mercX(clon);
    var cy = mercY(clat);
    var y = mercY(nlat) - cy;
    var x = mercX(nlon) - cx;
    if(x<-width/2){
        x = x + width; 
    }
    ellipse(x,y,mag);
}
