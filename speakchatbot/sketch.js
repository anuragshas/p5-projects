let continuous = true;
let interim = false;
let bot;
let bvoice;
let hvoice;
let reply;
let mic;
let fft;
let micHistory =[];

function bot_done (batch_num) {
	console.log("Batch #" + batch_num + " has finished loading!");

	// Now the replies must be sorted!
	bot.sortReplies();
}

// It's good to catch errors too!
function bot_error (error) {
	console.log("Error when loading files: " + error);
}
function setup() {
	createCanvas(400,400);
    bvoice = new p5.Speech(); // speech synthesis object
    hvoice = new p5.SpeechRec('en-IN',gotSpeech); // speech recognition object (will prompt for mic access)
    bot = new RiveScript();
    bot.loadFile([
        "brain.rive"
    ], bot_done, bot_error);
    hvoice.start(continuous, interim);
    mic = new p5.AudioIn()
    mic.amp(0.7);
    mic.start();
    fft = new p5.FFT(0,256);
    fft.setInput(mic);
}

function gotSpeech() {
    if (hvoice.resultValue) {
        let input = hvoice.resultString;
        let reply = bot.reply("local-user", input);
        bvoice.speak(reply);
        console.log(input);
        console.log("bot: "+reply);
        var pinput = createP(input);
        pinput.style("margin-left","50px");
        pinput.style("background-color","#E57373");
        pinput.style("width","350px");
        var preply = createP("bot: "+reply);
        preply.style("background-color","#64B5F6");
        preply.style("width","400px");
    }
}

function drawRotatingVisualiztion(micLevel){
    var maxRadius = width/2;
    micHistory.push(micLevel);
    angleMode(DEGREES);
    noFill();
    stroke(255);
    translate(width/2,height/2);
    beginShape();
    for(var i=0;i<360;i++){
        var r = map(micHistory[i],0,1,50,maxRadius);
        var x = r*cos(i);
        var y = r*sin(i);
        vertex(x,y);
    }
    endShape();
    if(micHistory.length >360){
        micHistory.splice(0,1);
    }
}

function drawPulse(micLevel){
    var maxRadius = width/2;
    var maxColor =255;
    fill(constrain(micLevel*maxColor, 0, maxColor),constrain(micLevel*maxColor*5, 0, maxColor),constrain(micLevel*maxColor*10, 0, maxColor));
    ellipse(width/2, height/2, constrain(micLevel*maxRadius*5, 0, maxRadius));
}

function drawVisualization(){
    var spectrum = fft.analyze();
    noStroke();
    fill(0,255,0); // spectrum is green
    resetMatrix();
    for (var i = 0; i< spectrum.length; i++){
        var x = map(i, 0, spectrum.length, 0, width);
        var h = -height + map(spectrum[i], 0, 255, height, 0);
        rect(x, height, width / spectrum.length, h )
    }
}

function draw() {
    background(0);
    var micLevel = mic.getLevel(0.5);
    drawPulse(micLevel);
    //drawRotatingVisualiztion(micLevel);
    //drawVisualization();
}
