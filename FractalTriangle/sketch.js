let points = [[0, 500], [250, 0], [500, 500]];
let t=[];

function setup() {
    createCanvas(500,500);
}

function draw() {
            newTriangles(points,floor(random(1,8)));
}

function getMid(p1,p2){
    return [ (p1[0]+p2[0]) / 2, (p1[1] + p2[1]) / 2];
}

function newTriangles(newPoints,depth){
        if(depth>0){
            newTriangles([newPoints[0],getMid(newPoints[0], newPoints[1]),getMid(newPoints[0], newPoints[2])],depth-1);
            newTriangles([newPoints[1],getMid(newPoints[0], newPoints[1]),getMid(newPoints[1], newPoints[2])],depth-1);
            newTriangles([newPoints[2],getMid(newPoints[2], newPoints[1]),getMid(newPoints[0], newPoints[2])],depth-1);
        }
        let fractri = new FractalTriangle(newPoints);
        stroke(floor(random(0,255)),floor(random(0,255)),floor(random(0,255)));
        noFill();
        fractri.makeTriangle();
}

class FractalTriangle{

    constructor(points){
    this.points=points;
 }
    makeTriangle(){
        triangle(this.points[0][0],this.points[0][1],this.points[1][0],this.points[1][1],this.points[2][0],this.points[2][1]);
    }
}
