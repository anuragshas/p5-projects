//https://en.wikipedia.org/wiki/L-system

var angle;
var axiom = "F";
var sentence = axiom;
var len = 100;

var rules = [];
rules[0] = {
  a: "F",
  b: "0FF+[1+F-F-F]-[2-F+F+F]"
}

var colors = [[83,53,10],[77, 158, 58],[58, 95, 11]];

function generate() {
  len *= 0.5;
  var nextSentence = "";
  for (var i = 0; i < sentence.length; i++) {
    var current = sentence.charAt(i);
    var found = false;
    for (var j = 0; j < rules.length; j++) {
      if (current == rules[j].a) {
        found = true;
        nextSentence += rules[j].b;
        break;
      }
    }
    if (!found) {
      nextSentence += current;
    }
  }
  sentence = nextSentence;
  //createP(sentence);
  turtle();

}

function turtle() {
  background(255);
  resetMatrix();
  translate(width / 2, height);
  stroke(255, 100);
  for (var i = 0; i < sentence.length; i++) {
    var current = sentence.charAt(i);

    if (current == "F") {
      line(0, 0, 0, -len);
      translate(0, -len);
    } else if (current == "+") {
      rotate(angle);
    } else if (current == "-") {
      rotate(-angle)
    } else if (current == "[") {
      push();
    } else if (current == "]") {
      pop();
    } else if (current == "0"){
        stroke(colors[0]);
    } else if (current == "1"){
        stroke(colors[1]);
    } else if (current == "2"){
        stroke(colors[2]);
    }
  }
}

function setup() {
  createCanvas(400, 400);
  angle = radians(20);
  background(51);
  createP(axiom);
  turtle();
  var button = createButton("generate");
  button.mousePressed(generate);
}
