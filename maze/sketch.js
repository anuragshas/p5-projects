let spc=10;
let grid = [];
let visitStack = [];
let current;
let cell;
let rows,cols;
const TOP = 0;
const RIGHT = 1;
const BOTTOM = 2;
const LEFT = 3;
let p;

function setup() {
	createCanvas(600,600);
	rows = floor(height/spc);
	cols = floor(width/spc);
    for(let i=0;i<rows;i++){
        for(let j=0;j<cols;j++){
            cell= new Cell(i,j);
            grid.push(cell);
        }
    }
    current = grid[0];
    p=createP('');
}

function draw() {
    background(51);
    current.visited = true;
    current.highlight();
    for(let i=0;i<grid.length;i++){
        grid[i].show();
    }
    let next = current.checkNeighbours();
    if(next){
        next.visited = true;
        visitStack.push(current);
        removeWalls(current,next);
        current =next;
    }
    else if(visitStack.length>0){
        current = visitStack.pop();
    }
    p.html(round(frameRate()));
}

function index(i,j){
    if(i<0||j<0||i>rows-1||j>cols-1){
        return -1;
    }
    else{
        return i*cols+j;
    }
}

function removeWalls(a,b){
    let xdiff = a.i-b.i;
    let ydiff = a.j-b.j;
    const TOP = 0;
    const RIGHT = 1;
    const BOTTOM = 2;
    const LEFT = 3;
    if(xdiff===-1){
        a.walls[RIGHT] = false;
        b.walls[LEFT] = false;
    }
    else if(xdiff===1){
        a.walls[LEFT] = false;
        b.walls[RIGHT] = false;
    }
    if(ydiff===-1){
        a.walls[BOTTOM] = false;
        b.walls[TOP] = false;
    }
    else if(ydiff===1){
        a.walls[TOP] = false;
        b.walls[BOTTOM] = false;
    }
}

class Cell {
    constructor (i,j){
        this.i = i;
        this.j = j;
        this.walls = [true,true,true,true];
        this.visited = false;
        this.neighbours = [];
    }
    
    highlight(){
        var x = this.i*spc;
        var y = this.j*spc;
        noStroke();
        fill(0,255,255);
        rect(x,y,spc,spc);
    }
    
    checkNeighbours(){
        let pos = [];
        this.neighbours = [];
        pos[TOP] = grid[index(this.i,this.j-1)];
        pos[RIGHT] = grid[index(this.i+1,this.j)];
        pos[BOTTOM] = grid[index(this.i,this.j+1)];
        pos[LEFT] = grid[index(this.i-1,this.j)];
        for(let i=0;i<pos.length;i++){
            if(pos[i] && !pos[i].visited){
                this.neighbours.push(pos[i]);
            }
        }
        if(this.neighbours.length>0){
            return random(this.neighbours);
        }
        else{
            return undefined;
        }
    }
    
    show(){
        var x = this.i*spc;
        var y = this.j*spc;
        stroke(255);
        if(this.walls[TOP]){
            line(x,y,x+spc,y);
        }
        if(this.walls[RIGHT]){
            line(x+spc,y,x+spc,y+spc);
        }
        if(this.walls[BOTTOM]){
            line(x+spc,y+spc,x,y+spc);
        }
        if(this.walls[LEFT]){
            line(x,y,x,y+spc);
        }
        if(this.visited){
                noStroke();
                fill(255,0,255,100);
                rect(x,y,spc,spc);
            }
    }
}
