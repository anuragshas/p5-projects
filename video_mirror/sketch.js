var video;
var slider;
var vScale = 5;
function setup() {
	createCanvas(640,480);
    background(51);
    pixelDensity(1);
	video = createCapture(VIDEO);
	video.size(width/vScale,height/vScale);
	//video.hide();
	slider = createSlider(0,255,70,1);
}

function draw() {
//    image(video, 0, 0);
    background(0);
    video.loadPixels();
    loadPixels();
    for(var y=0;y<video.height;y++){
        for(var x=0;x<video.width;x++){
            var index = (video.width - x + 1 +(y*video.width))*4;
            var r = video.pixels[index+0];
            var g = video.pixels[index+1];
            var b = video.pixels[index+2];
            var bright = (r*0.299+g*0.587+b*0.114);
            threshold = slider.value();
            /*if(bright>threshold){
                bright = 255;
            }
            else{
               bright = 0;
            }*/
            var w = map(bright,0,255,0,vScale);
            noStroke();
            fill(255);
            rectMode(CENTER);
            rect(x*vScale,y*vScale,w,w);
        }
    }
}
