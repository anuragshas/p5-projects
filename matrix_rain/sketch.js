let streams = [];
let symbolSize = 16;
let p;
function setup() {
    createCanvas(
    window.innerWidth,
    window.innerHeight);
    background(0);
    let x = 0;
    let stream;
    for(let i=0;i<width/symbolSize;i++){
        stream= new Stream(x);
        stream.generateSymbols();
        x+=symbolSize;
        streams.push(stream);
    }
    textSize(symbolSize);
    p =createP('');
}

function draw() {
    background(0,150);
    streams.forEach(function(stream){
        stream.render();
    });
    p.html(floor(frameRate()));
}

class Symbol{
    constructor(x,y,speed,first){
        this.x = x;
        this.y = y;
        this.speed = speed;
        this.value="";
        this.switchInterval = round(random(2,25));
        this.first = first
    }
    
    generateRandomSymbol(){
        if(frameCount % this.switchInterval === 0)
            this.value = String.fromCharCode(
            0x30A0 + round(random(0,96))
            );
    }
    
    rain(){
        this.y=this.y>height?0:this.y+=this.speed;
    }
}

class Stream{
    constructor(x){
        this.symbols = [];
        this.totalSymbols = round(random(5,35));
        this.speed = round(random(5,22));
        this.x = x;
    }
    
    generateSymbols(){
        let x = this.x;
        let y =round(random(-2000,0));
        let symbol;
        let first = round(random(0,4))===1;
        for(let i=0;i<this.totalSymbols;i++){
            symbol = new Symbol(x,y,this.speed,first);
            symbol.generateRandomSymbol();
            this.symbols.push(symbol);
            y-=symbolSize;
            first = false;
        }
    }
    
    render(){
        this.symbols.forEach(function(symbol){
            if(symbol.first){
                fill(180,255,180);
            }
            else{
                fill(0,255,70);
            }
            text(symbol.value, symbol.x,symbol.y);
            symbol.rain();
            symbol.generateRandomSymbol();
        });
    }
}

