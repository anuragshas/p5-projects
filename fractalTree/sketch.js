var reduction_factor=0;
var angle=0;
var reduction_factor_slider;
var angle_slider;

function setup() {
	createCanvas(400, 400);
	reduction_factor_slider = createSlider(0,0.72,0.67,0.01);
	angle_slider = createSlider(0,TWO_PI,PI/4,0.01);
}

function draw() {
    background(50);
    stroke(255);
    reduction_factor = reduction_factor_slider.value();
    angle = angle_slider.value();
    translate(height/2,height);
    tree(100);
}

function tree(len){
    line(0,0,0,-len);
    translate(0,-len);
    if(len>4){
        push();
        rotate(angle);
        strokeWeight(1);
        tree(len*reduction_factor);
        pop();
        push();
        rotate(-angle);
        strokeWeight(2);
        tree(len*reduction_factor);
        pop();
        }
}
